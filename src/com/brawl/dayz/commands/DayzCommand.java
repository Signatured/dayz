package com.brawl.dayz.commands;

import com.brawl.dayz.ZPlayer;

public abstract class DayzCommand {

	public abstract String name();
	public abstract String description();
	public abstract void run(String[] args, ZPlayer sender);
	public abstract boolean needsOp();
	
}