package com.brawl.dayz.commands;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.brawl.dayz.ZPlayer;

public class CommandManager implements CommandExecutor {

	public ArrayList<DayzCommand> commands = new ArrayList<DayzCommand>();
	
	public CommandManager() {
		//TODO: commands
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String l,
			String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			ZPlayer zp = ZPlayer.get(player.getUniqueId());
			
			
			if(args.length == 0) {
				for(DayzCommand command : commands) {
					if(command.needsOp() && zp.isOp()) {
						player.sendMessage("\247a/dayz " + command.name() + "\247d - " + command.description());
					}
					
					if(!command.needsOp()) {
						player.sendMessage("\247a/dayz " + command.name() + "\247d - " + command.description());
					}
				}
				return true;
			}
			
			DayzCommand found = null;
			
			for(DayzCommand command : commands) {
				if(args[0].equalsIgnoreCase(command.name())) {
					found = command;
					break;
				}
			}
			
			if(found == null) {
				player.sendMessage("\247cError: Command not found. Type /dayz for a list of commands.");
				return true;
			}
			
			if(found.needsOp() && !zp.isOp()) {
				player.sendMessage("\247cError: Command not found. Type /dayz for a list of commands.");
				return true;
			}
			
			ArrayList<String> argslist = new ArrayList<String>();
			
			for(String s : args) {
				argslist.add(s);
			}
			
			argslist.remove(0);
			
			String[] newargs = argslist.toArray(new String[argslist.size()]);
			
			found.run(newargs, zp);
		}
		return false;
	}

	
	
}