package com.brawl.dayz.commands;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.brawl.dayz.ZPlayer;
import com.brawl.dayz.util.ChatUtil;


public class ChannelCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("channel")) {
			if (sender instanceof Player) {
				Player s = (Player) sender;
				
				if (ZPlayer.get(s.getUniqueId()) != null) {
					ZPlayer player = ZPlayer.get(s.getUniqueId());
					
					if (args.length == 0) 
						s.sendMessage(ChatUtil.getPrefix() + "Your current channel: " + player.getChannel().toString());
					else if (args.length == 1) {
						if (args[0].equalsIgnoreCase("global")) {
							if (!player.getChannel().equals(ZPlayer.ChatChannel.GLOBAL)) {
								player.setChannel(ZPlayer.ChatChannel.GLOBAL);
								Bukkit.getPlayer(player.getUUID()).sendMessage(ChatUtil.getPrefix() + "Chat channel set to global!");
							} else
								Bukkit.getPlayer(player.getUUID()).sendMessage(ChatUtil.getPrefix() + "You're already speaking in global chat!");
						} else if (args[0].equalsIgnoreCase("local")) {
							if (!player.getChannel().equals(ZPlayer.ChatChannel.LOCAL)) {
								player.setChannel(ZPlayer.ChatChannel.LOCAL);
								Bukkit.getPlayer(player.getUUID()).sendMessage(ChatUtil.getPrefix() + "Chat channel set to local!");
							} else
								Bukkit.getPlayer(player.getUUID()).sendMessage(ChatUtil.getPrefix() + "You're already speaking in local chat!");
						}
					}
				}
			}
		}
		return true;
	}
}