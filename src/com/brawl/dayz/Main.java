package com.brawl.dayz;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

import com.brawl.dayz.carepackage.Carepackage;
import com.brawl.dayz.carepackage.Item;
import com.brawl.dayz.carepackage.PackageTask;
import com.brawl.dayz.carepackage.ParticleTask;
import com.brawl.dayz.carepackage.CarepackageMain;
import com.brawl.dayz.util.DBUtil;
import com.brawl.dayz.util.FileHandler;

public class Main extends JavaPlugin {
	
	//TODO: use actual database information
	public static DBUtil db;
	public static Main instance;
	public static FileHandler fh;
	
	public void onEnable() {
		instance = this;
		fh = new FileHandler();
		db = new DBUtil(fh.getIp(), 3306, fh.getUser(), fh.getPassword(), fh.getDatabase());
		
		for(String s : fh.getConfig().getStringList("carepackage")) {
			String[] ar = s.split(",");
			String name = ar[0];
			int amount = Integer.valueOf(ar[1]);
			double chance = Double.valueOf(ar[2]);
			
			Item item = new Item(Material.getMaterial(name.toUpperCase()), amount);
			CarepackageMain.items.put(item, chance);
		}
		
		new WarzTask().runTaskTimer(this, 20L, 20L);
		new PackageTask().runTaskTimer(Main.instance, 5L, 5L);
		new ParticleTask().runTaskTimer(Main.instance, 5L, 5L);
		Bukkit.getScheduler().runTaskTimer(this, new Runnable() {
			public void run() {
				spawnRandomCarePackage();
			}
		}, 300L * 20L, 300L * 20L);
	}
	
	private void spawnRandomCarePackage() {
		//TODO: use zones or get random position on the map to determine a location
	}

	public void onDisable() {
		for(Carepackage cp : Carepackage.packages) {
			cp.remove();
			cp.l.getBlock().setType(Material.AIR);
		}
	}
}