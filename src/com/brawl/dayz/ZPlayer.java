package com.brawl.dayz;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ZPlayer {
	
	public static ArrayList<ZPlayer> playerObjects = new ArrayList<ZPlayer>();
	
	private UUID uuid;
	private ChatChannel channel;
	
	private String name;
	
	private int bloodLevel;
	private int thirstLevel;
	private int staminaLevel;
	private int tempLevel;
	private int playerKills;
	private int zombieKills;
	private int deaths;
	private int distanceRan;
	private int timePlayed;
	
	
	private boolean bleeding;
	private boolean brokenLegs;
	
	private boolean sweating;
	private boolean heatStroke;
	
	private boolean frostBite;
	private boolean hypothermia;
	
	private boolean poisoned = false;
	
	public ZPlayer(Player player) {		
		this.uuid = player.getUniqueId();
		this.name = player.getName();
		
		this.channel = ChatChannel.GLOBAL;
		
		this.bloodLevel = 5000;
		this.thirstLevel = 1000;
		this.staminaLevel = 1000;
		this.tempLevel = 500;
		
		this.bleeding = false;
		this.brokenLegs = false;
		
		this.sweating = false;
		this.heatStroke = false;
		
		this.frostBite = false;
		this.hypothermia = false;
		
		this.poisoned = false;
		
		ResultSet set = Main.db.search("select * from dayz_stats where uuid='" + this.uuid + "'");
		try {
			if(set.next()) {
				this.playerKills = set.getInt("playerKills");
				this.zombieKills = set.getInt("zombieKills");
				this.timePlayed = set.getInt("timePlayed");
				this.distanceRan = set.getInt("distanceRan");
				this.deaths = set.getInt("deaths");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		playerObjects.add(this);
	}
	
	public Player getPlayer() {
		return Bukkit.getPlayer(this.uuid);
	}
	
	public boolean isOp() {
		return getPlayer().isOp();
	}
	
	public UUID getUUID() {
		return this.uuid;
	}
	
	public void setUUID(UUID uuid) {
		this.uuid = uuid;
	}
	
	public ChatChannel getChannel() {
		return this.channel;
	}
	
	public void setChannel(ChatChannel channel) {
		this.channel = channel;
	}
	
	public int getBloodLevel() {
		return this.bloodLevel;
	}
	
	public void setBloodLevel(int bloodLevel) {
		this.bloodLevel = bloodLevel;
	}
	
	public int getThristLevel() {
		return this.thirstLevel;
	}
	
	public void setThirstLevel(int thirstLevel) {
		this.thirstLevel = thirstLevel;
	}
	
	public int getStaminaLevel() {
		return this.staminaLevel;
	}
	
	public void setStaminaLevel(int staminaLevel) {
		this.staminaLevel = staminaLevel;
	}
	
	public int getTempLevel() {
		return this.tempLevel;
	}
	
	public void setTempLevel(int tempLevel) {
		this.tempLevel = tempLevel;
	}
	
	public boolean isBleeding() {
		return this.bleeding;
	}
	
	public void setBleeding(boolean b) {
		this.bleeding = b;
	}
	
	public boolean legsBroken() {
		return this.brokenLegs;
	}
	
	public void setLegsBroken(boolean b) {
		this.brokenLegs = b;
	}
	
	public boolean isSweating() {
		return this.sweating;
	}
	
	public void setSweating(boolean b) {
		this.sweating = b;
	}
	
	public boolean hasHeatStroke() {
		return this.heatStroke;
	}
	
	public void setHeatStroke(boolean b) {
		this.heatStroke = b;
	}
	
	public boolean hasFrostBite() {
		return this.frostBite;
	}
	
	public void setFrostBite(boolean b) {
		this.frostBite = b;
	}
	
	public boolean hasHypothermia() {
		return this.hypothermia;
	}
	
	public void setHypothermia(boolean b) {
		this.hypothermia = b;
	}
	
	public boolean isPoisoned() {
		return this.poisoned;
	}
	
	public void setPoisoned(boolean b) {
		this.poisoned = b;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDeaths() {
		return deaths;
	}

	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}

	public int getDistanceRan() {
		return distanceRan;
	}

	public void setDistanceRan(int distanceRan) {
		this.distanceRan = distanceRan;
	}

	public int getPlayerKills() {
		return playerKills;
	}

	public void setPlayerKills(int playerKills) {
		this.playerKills = playerKills;
	}

	public int getTimePlayed() {
		return timePlayed;
	}

	public void setTimePlayed(int timePlayed) {
		this.timePlayed = timePlayed;
	}

	public int getZombieKills() {
		return zombieKills;
	}

	public void setZombieKills(int zombieKills) {
		this.zombieKills = zombieKills;
	}
	
	public enum ChatChannel {
		LOCAL, GLOBAL, TEAM;
	}

	public static ZPlayer get(UUID uniqueId) {
		for(ZPlayer zp : playerObjects) {
			if(zp.getUUID().equals(uniqueId)) return zp;
		}
		
		return null;
	}
}