package com.brawl.dayz;

import org.bukkit.scheduler.BukkitRunnable;


public class WarzTask extends BukkitRunnable {

	public void run() {
		for (ZPlayer zp : ZPlayer.playerObjects) {
			zp.setTimePlayed(zp.getTimePlayed() + 1);
			
			if(zp.isBleeding()) {
				zp.getPlayer().damage(2D);
			}
			
			if(zp.legsBroken() && zp.getPlayer().getWalkSpeed() == 0.2F) {
				zp.getPlayer().setWalkSpeed(0.1F);
			}
		}
	}

}