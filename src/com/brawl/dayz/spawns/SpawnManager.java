package com.brawl.dayz.spawns;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class SpawnManager {
	
	public static SpawnManager sm = new SpawnManager();
	public static SpawnManager getManager() { return sm; }
	
	public void createSpawn(Location loc) {
		new Spawn(loc);
	}
	
	public void sendSpawn(Player player) {
		if (Spawn.spawns.size() > 0) {
			player.setBedSpawnLocation(Spawn.spawns.get(new Random().nextInt(Spawn.spawns.size())).getLocation());
		}
	}
}