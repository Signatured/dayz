package com.brawl.dayz.spawns;
import java.util.ArrayList;

import org.bukkit.Location;

public class Spawn {
	
	public static ArrayList<Spawn> spawns = new ArrayList<Spawn>();
	
	private Location loc;
	
	public Spawn(Location loc) {
		this.loc = loc;
		
		spawns.add(this);
	}
	
	public Location getLocation() {
		return this.loc;
	}
	
	public void setLocation(Location loc) {
		this.loc = loc;
	}
}