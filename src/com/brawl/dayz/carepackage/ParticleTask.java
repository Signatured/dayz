package com.brawl.dayz.carepackage;

import org.bukkit.scheduler.BukkitRunnable;

public class ParticleTask extends BukkitRunnable {

	public void run() {
		for(Carepackage cpackage : Carepackage.packages) {
			if(cpackage.isFlashing()) {
				ParticleEffect.RED_DUST.display(cpackage.item.getLocation(), 1, 0, 1, 0, 50);
			}
		}
	}
	
}