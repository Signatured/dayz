package com.brawl.dayz.carepackage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

public class CarepackageMain implements Listener {

	ArrayList<String> cooldown = new ArrayList<String>();
	HashMap<String, BukkitTask> tasks = new HashMap<String, BukkitTask>();
	ArrayList<Location> chests = new ArrayList<Location>();
	public static HashMap<com.brawl.dayz.carepackage.Item, Double> items = new HashMap<com.brawl.dayz.carepackage.Item, Double>();
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();

		if ((p.getItemInHand().getType() != Material.NETHER_STAR))
			return;

		p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);

		if (p.getItemInHand().getAmount() <= 1)
			p.getInventory().removeItem(p.getItemInHand());

		new Carepackage(p.getEyeLocation());
	}
	
	@EventHandler
	public void onPlayerChest(PlayerInteractEvent e) {
		
		if(!(e.getAction() == Action.RIGHT_CLICK_BLOCK)) return;
		
		if(e.getClickedBlock() == null) return;
		
		if(e.getClickedBlock().getType() == null) return;
		
		if(!(e.getClickedBlock().getType() == Material.CHEST)) return;
		
		Block b = e.getClickedBlock();
		
		if(!b.hasMetadata("loc")) return;
		
		boolean bo = b.getMetadata("loc").get(0).asBoolean();
		
		if(bo) {
			e.getClickedBlock().setType(Material.AIR);
			
			dropItems(e.getClickedBlock().getLocation());
			
			e.setCancelled(true);
		}
	}
	
	private void dropItems(Location location) {
		List<ItemStack> droppableItems = new ArrayList<ItemStack>();
		
		for(com.brawl.dayz.carepackage.Item item : items.keySet()) {
			double chance = items.get(item);
			double real = new Random().nextDouble();
			
			if(chance >= real) {
				droppableItems.add(item.getItemStack());
			}
		}
		
		for(ItemStack item : droppableItems) {
			Item i = location.getWorld().dropItem(location, item);
			i.setPickupDelay(0);
		}
	}
}