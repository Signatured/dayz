package com.brawl.dayz.carepackage;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import com.brawl.dayz.Main;

public class Carepackage {

	public static ArrayList<Carepackage> packages = new ArrayList<Carepackage>();
	
	private Location loc;
	private boolean isFlashing = false, chestFalling = false;
	
	public Item item = null;
	
	public Location l;
	
	public Carepackage(Location loc) {
		this.loc = loc;
		startPackage();
		
		packages.add(this);
	}
	
	public void remove() {
		packages.remove(this);
	}
	
	public void startPackage() {
	    item = loc.getWorld().dropItem(loc, new ItemStack(Material.NETHER_STAR));
		item.setPickupDelay(1000);
		item.setVelocity(loc.getDirection().multiply(1.2D));
		item.setPickupDelay(8000);
		
		setIsFlashing(true);
		
		Bukkit.getScheduler().runTaskLater(Main.instance, new Runnable() {
			public void run() {
				spawnCarePackage(item);
				item.remove();
			}
		}, 20L * 10L);
	}

	private void spawnCarePackage(Item item) {
		setIsFlashing(false);
		setChestFalling(true);
		
		l = item.getLocation().add(0, 10, 0);
		l.getBlock().setType(Material.CHEST);
	}

	public void setChestFalling(boolean b) {
		chestFalling = b;
	}
	
	public boolean isChestFalling() {
		return chestFalling;
	}

	public Location getLocation() {
		return loc;
	}

	public boolean isFlashing() {
		return isFlashing;
	}
	
	public void setIsFlashing(boolean b) {
		isFlashing = b;
	}
	
}