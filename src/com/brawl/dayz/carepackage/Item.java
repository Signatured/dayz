package com.brawl.dayz.carepackage;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Item {

	private Material material;
	private int amount;
	
	public Item(Material material, int amount) {
		this.material = material;
		this.amount = amount;
	}
	
	public ItemStack getItemStack() {
		return new ItemStack(material, amount);
	}

}