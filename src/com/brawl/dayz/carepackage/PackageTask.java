package com.brawl.dayz.carepackage;

import org.bukkit.Material;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import com.brawl.dayz.Main;

public class PackageTask extends BukkitRunnable {

	public void run() {
		if(Carepackage.packages.size() > 0) {
			for(Carepackage cpackage : Carepackage.packages) {
				if(cpackage.isChestFalling()) {
					cpackage.l.getBlock().setType(Material.AIR);
					cpackage.l.subtract(0, 1, 0);
					if (cpackage.l.getBlock().getType() == Material.AIR) {
						cpackage.l.getBlock().setType(Material.CHEST);
					} else {
						cpackage.l.add(0, 1, 0).getBlock().setType(Material.CHEST);
						cpackage.l.getBlock().setMetadata("loc", new FixedMetadataValue(Main.instance, Boolean.valueOf(true)));
						cpackage.setChestFalling(false);
					}
				}
			}
		}
	}
}