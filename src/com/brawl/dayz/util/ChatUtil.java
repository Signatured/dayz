package com.brawl.dayz.util;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.brawl.dayz.ZPlayer;

public class ChatUtil {

	private static String prefix = "[DayZ] ";
	
	public static void sendMessage(String message, Player player) {
		player.sendMessage(prefix + message);
	}
	
	public static void broadcastMessage(String message) {
		for(Player player : Bukkit.getOnlinePlayers()) {
			player.sendMessage(message);
		}
	}
	
	public static void sendMessage(ZPlayer player, String message) {
		if (player.getChannel().equals(ZPlayer.ChatChannel.GLOBAL)) {
			for (Player online : Bukkit.getOnlinePlayers()) {
				if (ZPlayer.get(online.getUniqueId()) != null) {
					ZPlayer receiver = ZPlayer.get(online.getUniqueId());
					
					if (receiver.getChannel().equals(ZPlayer.ChatChannel.GLOBAL) || receiver.getChannel().equals(ZPlayer.ChatChannel.TEAM)) {
						online.sendMessage("[G] " + player.getName() + ": " + message);
					} else if (receiver.getChannel().equals(ZPlayer.ChatChannel.LOCAL)) {
						List<Entity> playersInRange = Bukkit.getPlayer(player.getUUID()).getNearbyEntities(25, 25, 25);
						
						for (Entity inRange : playersInRange) {
							if (inRange instanceof Player & inRange.getName().equals(online.getName()))
								online.sendMessage("[G] " + player.getName() + ": " + message);
						}
					}
				}
			}
		} else if (player.getChannel().equals(ZPlayer.ChatChannel.LOCAL)) {
			List<Entity> playersInRange = Bukkit.getPlayer(player.getUUID()).getNearbyEntities(25, 25, 25);
			
			for (Entity inRange : playersInRange) {
				if (inRange instanceof Player) 
					inRange.sendMessage("[L] " + player.getName() + ": " + message);
			}
		}
	}
	
	public static String getPrefix() {
		return prefix;
	}
	
}