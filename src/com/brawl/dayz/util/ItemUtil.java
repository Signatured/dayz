package com.brawl.dayz.util;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemUtil {

	public static ItemStack createNamedItem(String name, Material type, int amount, byte data) {
		ItemStack item = createItemStack(type, amount, data);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		item.setItemMeta(meta);
		
		return item;
	}
	
	public static ItemStack createItemStack(Material type, int amount, byte data) {
		return new ItemStack(type, amount, data);
	}
	
	public static ItemStack createNamedItem(String name, Material type, int amount, byte data, String... lore) {
		ItemStack stack = createNamedItem(name, type, amount, data);
		ItemMeta meta = stack.getItemMeta();
		meta.setLore(Arrays.asList(lore));
		stack.setItemMeta(meta);
		
		return stack;
	}
}