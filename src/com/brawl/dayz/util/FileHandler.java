package com.brawl.dayz.util;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.brawl.dayz.Main;

public class FileHandler {

	Main m = com.brawl.dayz.Main.instance;
	
	private File f;
	private YamlConfiguration config;
	public FileHandler() {
		try {
			if(!m.getDataFolder().exists())
				m.getDataFolder().mkdir();
			
			f = new File(m.getDataFolder(), "config.yml");
			if(!f.exists()) {
				f.createNewFile();
				loadDefaults();
			}
			
			config = YamlConfiguration.loadConfiguration(f);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadDefaults() {
		if(config == null) config = YamlConfiguration.loadConfiguration(f);
		
		set("ip", "localhost");
		set("user", "user");
		set("password", "password");
		set("database", "database");
		set("port", "port");
	}
	
	public void set(String path, Object o) {
		config.set(path, o);
		save();
	}
	
	public void save() {
		try {
			config.save(f);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getIp() {
		return config.getString("ip");
	}
	
	public String getUser() {
		return config.getString("user");
	}

	public String getPassword() {
		return config.getString("password");
	}
	
	public String getDatabase() {
		return config.getString("database");
	}
	
	public FileConfiguration getConfig() {
		return config;
	}
	
	public String getPort() {
		return config.getString("port");
	}
}