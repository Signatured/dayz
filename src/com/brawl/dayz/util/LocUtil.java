package com.brawl.dayz.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class LocUtil {

	public static String locToString(Location loc) {
		return loc.getWorld().getName() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ();
	}
	
	public static Location stringToLoc(String string) {
		try {
			String[] ar = string.split(",");
			World world = Bukkit.getWorld(ar[0]);
			double x = Double.valueOf(ar[1]);
			double y = Double.valueOf(ar[2]);
			double z = Double.valueOf(ar[3]);
			
			return new Location(world, x, y, z);
		}catch(Exception e) {
			System.err.println("Could not convert string > location. Returning default spawn location");
			return Bukkit.getWorlds().get(0).getSpawnLocation();
		}
	}
	
}