package com.brawl.dayz.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class DBUtil {

	private Connection connection;
	
	private String ip, user, password, database;
	private int port;
	
	public DBUtil(String ip, int port, String user, String password, String database) {
		this.setIp(ip);
		this.setPort(port);
		this.setUser(user);
		this.setPassword(password);
		this.setDatabase(database);
	}
	
	public Connection getConnection() {
		Properties cp = new Properties();
		cp.put("user", user);
		cp.put("password", password);
		try {
			this.connection = DriverManager.getConnection(
			        "jdbc:mysql://" +
			        this.ip +
			        ":" + this.port + "/",
			        cp);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return connection;
	}
	
	public void closeConnection() {
		if(this.connection != null)
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}

	public void createStatement(String query) {
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet search(String query) {
		try {
			PreparedStatement statement = connection.prepareStatement(query);
			
			ResultSet set = statement.executeQuery();
			
			set.close();
			statement.close();
			
			return set;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}
}