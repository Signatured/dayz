package com.brawl.dayz.listeners;

import org.bukkit.Bukkit;

import com.brawl.dayz.Main;
import com.brawl.dayz.carepackage.CarepackageMain;

public class ListenerManager {

	public ListenerManager() {
		Bukkit.getServer().getPluginManager().registerEvents(new Death(), Main.instance);
		Bukkit.getServer().getPluginManager().registerEvents(new Move(), Main.instance);
		Bukkit.getServer().getPluginManager().registerEvents(new Damage(), Main.instance);
		Bukkit.getServer().getPluginManager().registerEvents(new Cut(), Main.instance);
		Bukkit.getServer().getPluginManager().registerEvents(new CarepackageMain(), Main.instance);
	}
	
}