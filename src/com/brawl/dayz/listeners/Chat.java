package com.brawl.dayz.listeners;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.brawl.dayz.ZPlayer;
import com.brawl.dayz.util.ChatUtil;

public class Chat implements Listener {
	
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		if (ZPlayer.get(e.getPlayer().getUniqueId()) != null) {
			ZPlayer player = ZPlayer.get(e.getPlayer().getUniqueId());
			String message = e.getMessage();
			
			ChatUtil.sendMessage(player, message);
			e.setCancelled(true);
		}
	}
}