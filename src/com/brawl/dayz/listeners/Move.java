package com.brawl.dayz.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.brawl.dayz.ZPlayer;

public class Move implements Listener {

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		ZPlayer zplayer = ZPlayer.get(player.getUniqueId());
		
		if(event.getTo().getBlockX() == event.getFrom().getBlockX() && event.getTo().getBlockZ() == event.getFrom().getBlockZ() && event.getTo().getBlockY() == event.getFrom().getBlockY()) return;
		
		zplayer.setDistanceRan(zplayer.getDistanceRan() + 1);
	}
	
}