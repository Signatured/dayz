package com.brawl.dayz.listeners;

import java.util.Random;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.brawl.dayz.ZPlayer;

public class Damage implements Listener {

	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		Entity ent = event.getEntity();
		
		if(ent instanceof Player) {
			Player player = (Player) ent;
			ZPlayer zp = ZPlayer.get(player.getUniqueId());
			
			if(event.getCause() == DamageCause.FALL && !zp.legsBroken() && event.getDamage() > 6D) {
				zp.setLegsBroken(true);
				player.setWalkSpeed(0.1F);
			}
			
			if(!zp.isBleeding() && new Random().nextInt(100) < 30) {
				zp.setBleeding(true);
			}
		}
	}
	
}