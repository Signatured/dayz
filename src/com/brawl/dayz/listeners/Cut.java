package com.brawl.dayz.listeners;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Cut implements Listener {
	
	@EventHandler
	public void onBarbCut(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		ItemStack currentItem = player.getItemInHand();
		
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK && currentItem != null && currentItem.getType() != Material.AIR) {
			if (currentItem.getType() == Material.SHEARS && e.getClickedBlock().getType() == Material.WEB) {				
				e.getClickedBlock().setType(Material.AIR);
				currentItem.setDurability((short) (currentItem.getDurability() + 3));
								
				try {
					String line = currentItem.getItemMeta().getLore().get(0);
					line.replace("Uses left: ", "");
					int dura = Integer.parseInt(line);
					
					dura--;
					currentItem.getItemMeta().setLore(Arrays.asList(ChatColor.translateAlternateColorCodes('&', "&7Uses left: &a") + dura));
					currentItem.setDurability((short) (currentItem.getDurability() + 3.7));
					
				} catch (Exception ex) {
					currentItem.getItemMeta().setLore(Arrays.asList(ChatColor.translateAlternateColorCodes('&', "&7Uses left: &a49")));
					currentItem.setDurability((short) 234.3);
				}
				
				//64 Uses
			}
		}
	}
}