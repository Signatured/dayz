package com.brawl.dayz.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.brawl.dayz.ZPlayer;

public class Death implements Listener {

	//TODO: add other shit like auto respawn or whatever
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		Player dead = event.getEntity();
		Player killer = dead.getKiller();
		
		if(dead == killer) return;
		
		ZPlayer zdead = ZPlayer.get(dead.getUniqueId());
		ZPlayer zkiller = ZPlayer.get(killer.getUniqueId());
		
		zdead.setDeaths(zdead.getDeaths() + 1);
		zkiller.setPlayerKills(zkiller.getPlayerKills() + 1);
		
		dead.spigot().respawn();
		
	}
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event) {
		Entity dead = event.getEntity();
		LivingEntity ldead = (LivingEntity) dead;
		
		if(ldead.getKiller() instanceof Player && ldead.getType() == EntityType.ZOMBIE) {
			ZPlayer killer = ZPlayer.get(ldead.getKiller().getUniqueId());
			
			killer.setZombieKills(killer.getZombieKills() + 1);
		}
	}
		
}